// Takes in every route that is created from running 'yarn generate'
// and populates those links in the sitemap

export default function () {
  this.nuxt.hook('generate:done', (context) => {
    // Grabs every route that Nuxt generated when running 'yarn generate'
    const allRoutes = Array.from(context.generatedRoutes);

    const exclude = this.nuxt.options.sitemap.exclude;

    // Remove routes listed in the 'exclude' array from the sitemap module
    const filteredRoutes = allRoutes.filter((route) => {
      return !exclude.some((excludedPattern) =>
        route.includes(excludedPattern),
      );
    });

    this.nuxt.options.sitemap.routes = [...filteredRoutes];
  });
}
