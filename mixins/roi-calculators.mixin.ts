import Vue from 'vue';

export const calculatorMixin = Vue.extend({
  props: {
    tooltipData: {
      type: Object,
      required: false,
      default: null,
    },
    premiumFeatures: {
      type: Object,
      required: false,
      default: null,
    },
    ultimateFeatures: {
      type: Object,
      required: false,
      default: null,
    },
    spendTooltip: {
      type: String,
      required: false,
      default: null,
    },
  },
  watch: {
    '$route.query.step': function (newValue) {
      const number = Number(newValue);

      if (isNaN(number)) {
        this.currentStep = 0;
      } else {
        this.currentStep = number - 1;
      }
    },
  },
  mounted() {
    if (
      this.$route.query.step &&
      this.$route.query.step > 1 &&
      this.$route.query.step <= 2
    ) {
      this.currentStep = this.$route.query.step - 1;
    } else {
      this.currentStep = 0;
      this.$router.push({
        path: '',
        query: {
          ...this.$route.query,
          calculator: this.$route.query.calculator,
        },
      });
    }
    this.$emit('stepChange', this.currentStep);
  },
  methods: {
    nextStep() {
      this.currentStep += 1;
      this.$router.push({
        path: '',
        query: {
          ...this.$route.query,
          calculator: this.$route.query.calculator,
          step: this.currentStep + 1,
        },
      });
      this.$emit('stepChange', this.currentStep + 1);
    },
    selectStep(step, isCompleted) {
      if (isCompleted) {
        this.currentStep = step;
        this.$router.push({
          path: '',
          query: {
            ...this.$route.query,
            calculator: this.$route.query.calculator,
            step: step + 1,
          },
        });
        this.$emit('stepChange', this.currentStep);
      }
    },
  },
});
