import { Context } from '@nuxt/types';
import { FreeTrialService } from '../free-trial/index.service';
import { COMPONENT_NAMES } from '~/common/constants';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class SolutionsStartupsService {
  private readonly $ctx: Context;

  private readonly $freeTrialService: FreeTrialService;

  private readonly componentNames = {
    BY_SOLUTION_VALUE_PROP: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
    CUSTOMERS: 'education-case-study-carousel',
    CUSTOMER_LOGOS: COMPONENT_NAMES.CUSTOMER_LOGOS,
    CTA_BLOCK: COMPONENT_NAMES.CTA_BLOCK,
    SINGLE_CTA_BLOCK: COMPONENT_NAMES.SINGLE_CTA_BLOCK,
    FAQ: COMPONENT_NAMES.FAQ,
    HERO: COMPONENT_NAMES.SOLUTIONS_HERO,
    OVERVIEW: COMPONENT_NAMES.SOLUTIONS_VIDEO_FEATURE,
    SIDE_NAVIGATION: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
    STARTUPS_OVERVIEW: 'Solutions - Startups - Join. Startups Overview',
  };

  private readonly componentInternalNames = {
    BENEFITS: 'Solutions - Startups by solution value prop',
    STARTUPS_LINK: 'startups-link',
    CTA_BLOCK: 'Solutions - Startups CTA block',
    CUSTOMER_LOGOS: 'Solutions - Startups customers logo',
    CUSTOMERS: 'Solutions - Startups case study carousel',
    FORM: 'open-source-form-section',
    OVERVIEW: 'Solutions - Startups video feature',
    STARTUPS_INTRO: 'startups-intro',
    STARTUPS_OVERVIEW: 'startups-overview',
  };

  constructor($context: Context) {
    this.$ctx = $context;
    this.$freeTrialService = new FreeTrialService(this.$ctx);
  }

  /**
   * Main method that returns components to the /solutions/startups/* pages
   * @param slug
   * @param isIndex
   */
  async getContent(slug: string, isIndex: boolean = false) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      try {
        // FOR now, landing startups page has sideNav,
        // child pages DO NOT have sideNav
        return this.getContentfulData(slug, isIndex);
      } catch (e) {
        throw new Error(e);
      }
    }

    try {
      const content = this.$ctx
        .$content(this.$ctx.i18n.locale, isIndex ? `${slug}/index` : `${slug}`)
        .fetch();
      // Localized data - YML files
      return content;
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string, hasSideNav: boolean) {
    try {
      const content = await getClient().getEntries({
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      });

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [solutions] = content.items;

      return this.transformContentfulData(solutions, hasSideNav);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async transformContentfulData(ctfData: any, hasSideNav: boolean) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    if (hasSideNav) {
      const [sideNav]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.SIDE_NAVIGATION,
      );

      const [hero]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.HERO,
      );

      const [customerLogos]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.CUSTOMER_LOGOS,
      );

      const [ctaBlock]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.CTA_BLOCK,
      );

      const otherComponents = mappedContent.filter(
        (component) =>
          component.name !== this.componentNames.HERO &&
          component.name !== this.componentNames.CUSTOMER_LOGOS &&
          component.name !== this.componentNames.CTA_BLOCK &&
          component.name !== this.componentNames.SIDE_NAVIGATION,
      );

      const newSideNav = {
        name: this.componentNames.SIDE_NAVIGATION,
        slot_enabled: true,
        links: sideNav.links,
        slot_content: [...otherComponents],
      };

      const newArray = [];

      newArray.push(hero);
      newArray.push(customerLogos);
      newArray.push(newSideNav);
      newArray.push(ctaBlock);

      const transformedPageWithSideNav = {
        ...seoMetadata[0]?.fields,
        title: seoMetadata[0]?.fields.ogTitle,
        template: 'industry',
        components: newArray,
      };

      return transformedPageWithSideNav;
    }

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      pageContent,
      components: mappedContent,
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      const mappedComponent = this.mapCtfComponent(ctfComponent);
      components.push(mappedComponent);
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHero(ctfComponent);
        break;
      }

      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroup(ctfComponent);
        break;
      }

      case CONTENT_TYPES.FAQ: {
        component = this.mapFaq(ctfComponent);
        break;
      }

      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapHeaderAndText(ctfComponent);
        break;
      }

      case CONTENT_TYPES.TWO_COLUMN_BLOCK: {
        component = this.mapForm(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD: {
        component = this.mapCard(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  mapSideNavigation(ctfSideNavigation: any) {
    const { anchors } = ctfSideNavigation.fields;

    return {
      name: this.componentNames.SIDE_NAVIGATION,
      slot_enabled: true,
      links: anchors.map((anchor) => {
        return {
          title: anchor.fields.linkText,
          href: anchor.fields.anchorLink,
        };
      }),
    };
  }

  private mapHero(ctfHero: any) {
    const { title, subheader, primaryCta, backgroundImage } = ctfHero.fields;

    return {
      name: this.componentNames.HERO,
      data: {
        title,
        subtitle: subheader,
        aos_animation: 'fade-down',
        aos_duration: 500,
        title_variant: 'display1',
        mobile_title_variant: 'heading1-bold',
        img_animation: 'zoom-out-left',
        img_animation_duration: 1600,
        primary_btn: {
          url: primaryCta.fields.externalUrl,
          text: primaryCta.fields.text,
          data_ga_name: primaryCta.fields.dataGaName,
          data_ga_location: primaryCta.fields.dataGaLocation,
        },
        image: {
          bordered: true,
          image_url: backgroundImage.fields.file.url,
          alt: '',
        },
      },
    };
  }

  private mapCardGroup(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.fields.internalName.trim()) {
      case this.componentInternalNames.CUSTOMER_LOGOS:
        component = this.mapCustomerLogos(ctfCardGroup);
        break;

      case this.componentInternalNames.OVERVIEW:
        component = this.mapVideoFeature(ctfCardGroup);
        break;

      case this.componentInternalNames.BENEFITS:
        component = this.mapBySolutionValueProp(ctfCardGroup);
        break;

      case this.componentInternalNames.CUSTOMERS:
        component = this.mapCaseStudyCarousel(ctfCardGroup);
        break;

      case this.componentInternalNames.CTA_BLOCK:
        component = this.mapCtaBlock(ctfCardGroup);
        break;

      case this.componentNames.STARTUPS_OVERVIEW:
        component = this.mapStartupsOverview(ctfCardGroup);
        break;

      default:
        break;
    }
    return component;
  }

  private mapCustomerLogos(ctfCustomerLogos: any) {
    const { card, customFields } = ctfCustomerLogos.fields;

    return {
      name: this.componentNames.CUSTOMER_LOGOS,
      data: {
        text_variant: customFields.text_variant,
        companies: card.map((logo) => {
          return {
            image_url: logo.fields.image.fields.file.url,
            link_label: logo.fields.button.fields.text,
            alt: logo.fields.title,
            url: logo.fields.button.fields.externalUrl,
          };
        }),
      },
    };
  }

  private mapVideoFeature(ctfVideoFeature: any) {
    const { title, description, video } = ctfVideoFeature.fields.card[0].fields;
    return {
      name: 'div',
      id: 'overview',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.OVERVIEW,
          data: {
            header: title,
            description,
            video: {
              url: video.fields.url,
            },
          },
        },
      ],
    };
  }

  private mapCaseStudyCarousel(ctfCaseStudyCarousel: any) {
    const {
      header,
      cta,
      card: cards,
      componentName,
    } = ctfCaseStudyCarousel.fields;

    return {
      name: 'div',
      id: componentName || 'customers',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.CUSTOMERS,
          data: {
            header,
            customers_cta: true,
            cta: {
              href: cta.fields.externalUrl,
              text: cta.fields.text,
              data_ga_name: cta.fields.dataGaName,
            },
            case_studies: cards.map((card) => {
              return {
                logo_url: card.fields.image.fields.file.url,
                instituion_name:
                  card.fields.contentArea[0].fields.authorCompany,
                quote: {
                  img_url:
                    card.fields.contentArea[0].fields.authorHeadshot.fields
                      .image.fields.file.url,
                  quote_text: card.fields.contentArea[0].fields.quoteText,
                  author: card.fields.contentArea[0].fields.author,
                  author_title: card.fields.contentArea[0].fields.authorTitle,
                },
                case_study_url: card.fields.cardLink,
                data_ga_name: card.fields.cardLinkDataGaName,
                data_ga_location: card.fields.cardLinkDataGaLocation,
              };
            }),
          },
        },
      ],
    };
  }

  mapCtaBlock(ctfCtaBlock: any, isSingleCtaBlock: boolean = false) {
    return {
      name:
        (isSingleCtaBlock && this.componentNames.SINGLE_CTA_BLOCK) ||
        this.componentNames.CTA_BLOCK,
      data: {
        cards: ctfCtaBlock.fields.card.map((card) => {
          return {
            header: card.fields.title,
            icon: card.fields.iconName,
            link: {
              text: card.fields.button.fields.text,
              url: card.fields.button.fields.externalUrl,
              data_ga_name: card.fields.button.fields.dataGaName,
            },
          };
        }),
      },
    };
  }

  mapBySolutionValueProp(ctfBySolutionValueProp: any) {
    const {
      header,
      componentName,
      card: cards,
      customFields,
    } = ctfBySolutionValueProp.fields;

    return {
      name: 'div',
      id: componentName || 'benefits',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.BY_SOLUTION_VALUE_PROP,
          data: {
            title: header,
            componentName,
            light_background: customFields.light_background,
            large_card_on_bottom: customFields.large_card_on_bottom,
            cards: cards.map((card) => {
              const { title, description, iconName } = card.fields;

              const data = {
                title,
                description,
                icon: {
                  name: iconName,
                  alt: `${iconName} icon`,
                  variant: 'marketing',
                },
              };

              if (card.fields.button) {
                return {
                  ...data,
                  href: card.fields.button.fields.externalUrl,
                  data_ga_name: card.fields.button.fields.dataGaName,
                  cta: card.fields.button.fields.text,
                };
              }

              if (card.fields.cardLink) {
                return {
                  ...data,
                  href: card.fields.cardLink,
                  data_ga_name: card.fields.cardLinkDataGaName,
                };
              }

              return data;
            }),
          },
        },
      ],
    };
  }

  private mapFaq(ctfFaq: any) {
    const component = this.$freeTrialService.mapFaq(ctfFaq);

    return {
      name: this.componentNames.FAQ,
      data: component,
    };
  }

  private mapHeaderAndText(ctfHeaderAndText: any) {
    const { header, text } = ctfHeaderAndText.fields;

    return {
      name: this.componentInternalNames.STARTUPS_INTRO,
      data: {
        title: header,
        subtitle: text,
      },
    };
  }

  private mapForm(ctfForm: any) {
    const { header, componentId, text, blockGroup } = ctfForm.fields;
    const form = blockGroup[0].fields;

    return {
      name: this.componentInternalNames.FORM,
      data: {
        title: header,
        id: componentId,
        blocks: [{ content: text }],
        form: {
          formId: form.formId,
          form_header: '',
          datalayer: form.formDataLayer,
        },
      },
    };
  }

  private mapCard(ctfCard: any) {
    const { title, description, button, image, iconName } = ctfCard.fields;

    return {
      name: this.componentInternalNames.STARTUPS_LINK,
      data: {
        header: title,
        description,
        button: {
          href: button.fields.externalUrl,
          text: button.fields.text,
        },
        image: image.fields.file.url,
        alt: image.fields.title,
        icon: iconName,
      },
    };
  }

  private mapStartupsOverview(ctfStartupsOverview: any) {
    const { header, description, card: cards } = ctfStartupsOverview.fields;

    const blocks = [];

    const lastBlockIndex = cards.findIndex(
      (card) => card.sys.id === '6smEjCS0baaHD4Du5jOIOt',
    );

    const firstBlock = cards.slice(0, lastBlockIndex);
    const lastBlock = cards.slice(lastBlockIndex);

    const firstBlockHeader = cards.find(
      (card) => card.sys.id === '4t3QqoUOndpd5Zt45RTLhy',
    );
    const lastBlockHeader = cards.find(
      (card) => card.sys.id === '6smEjCS0baaHD4Du5jOIOt',
    );

    // BUILD FIRST BLOCK
    const newFirstBlock: any = {
      header: firstBlockHeader.fields.title,
      offers: [],
    };

    firstBlock.forEach((entry) => {
      const isFirstBlock =
        firstBlock.includes(entry) && entry === firstBlockHeader;

      if (isFirstBlock) {
        newFirstBlock.offers.push({
          list: entry.fields.list,
        });
      } else {
        newFirstBlock.offers.push({
          title: entry.fields.title,
          list: entry.fields.list,
        });
      }
    });
    blocks.push(newFirstBlock);

    // BUILD SECOND BLOCK
    const newLastBlock: any = {
      header: lastBlockHeader.fields.title,
      offers: [],
    };

    // REMOVE ADDITIONAL INFO SECTION, ADDED LATER
    lastBlock.pop();

    lastBlock.forEach((entry) => {
      const isLastBlock =
        lastBlock.includes(entry) && entry === lastBlockHeader;

      if (isLastBlock) {
        newLastBlock.offers.push({
          list: entry.fields.list,
        });
      } else {
        newLastBlock.offers.push({
          title: entry.fields.title,
          list: entry.fields.list,
        });
      }
    });

    blocks.push(newLastBlock);

    // FIND ADDITIONAL REQUIREMENTS SECTION
    const [additionalRequirements] = cards.filter(
      (entry) => entry.sys.id === '2DOOH6aMLwzNha5TTBwRwk',
    );

    const informationBlock = {
      header: additionalRequirements.fields.title,
      list: additionalRequirements.fields.list,
    };

    const result = {
      name: this.componentInternalNames.STARTUPS_OVERVIEW,
      data: {
        header,
        description,
        blocks,
        information: informationBlock,
      },
    };

    return result;
  }
}
